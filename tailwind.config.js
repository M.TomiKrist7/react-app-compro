/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors:{
        bee:'#fcd423',
        coklat:'#6a3c2c',
      },
      backgroundColor:{
        bee:'#fcd423',
        coklat:'#6a3c2c',
      },
      textColor:{
        bee:'#fcd423',
        coklat:'#6a3c2c',
      },
      screens: {
        'kcl':{'min':'250px','max':'479px'},
        sm: '480px',
        nb:'888px',

      },
      backgroundImage:{
          'about':"url('/assets/img/background.jpg')"
      },
    },
  },
  plugins: [],
}
