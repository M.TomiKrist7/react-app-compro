import React from 'react'
import { BsInstagram } from "react-icons/bs";
import { BsFacebook } from "react-icons/bs";
import { BsGithub } from "react-icons/bs";
import { BsWhatsapp } from "react-icons/bs";
import { SiGmail } from "react-icons/si";

function footer() {
  return (
    <footer className='bg-bee xl:h-56 lg:h-56 mb:h-[30rem] sm:h-[38rem] kcl:h-[38rem]'>
      <div className='bg-bee'>
          <div className=' h-[85%] px-10'>
            <div className='flex  lg:justify-between md:justify-center sm:justify-center lg:gap-8 md:gap-8 sm:gap-4 justify-center items-center flex-wrap h-full g-1'>
              <div className='p-4'>
                <div className='text-coklat font-bold text-lg text-center mb-3'>Anggota Dari: </div>
                <img className='h-32 w-32 m-auto' src='/icon.png'></img>
                <span className='text-coklat font-bold text-lg text-center'>PT. Let Eat Bee Multi Perkasa</span>
              </div>
              <div className='flex flex-col space-y-8 p-4'>
                <div className='flex '>
                  <BsInstagram className='text-3xl text-coklat font-medium'/>
                  <span className='text-xl text-coklat font-medium ml-4'>@LET.EATBEE_</span>
                </div>
                <div className='flex '>
                  <BsFacebook className='text-3xl text-coklat font-medium'/>
                  <span className='text-xl text-coklat font-medium ml-4'>LET EAT BEE</span>
                </div>
                <div className='flex '>
                  <BsGithub className='text-3xl text-coklat font-medium'/>
                  <span className='text-xl text-coklat font-medium ml-4'>Tm.skristanto7</span>
                </div>
              </div>
              <div className='flex flex-col space-y-9 xl:-m-2 lg:-m-2 sm:mt-6  kcl:mt-5 p-4' >
                <div className='text-xl text-coklat font-medium lg:m-0 sm:m-auto kcl:m-auto mb-4'>Order Hubungi:</div>
                <div>
                  <div className='flex mb-3'>
                    <BsWhatsapp className='text-3xl text-coklat font-medium'/>
                    <span className='text-xl text-coklat font-medium ml-4'>+62 877-574-545-49</span>
                  </div>
                  <div className='flex mb-3'>
                    <SiGmail className='text-3xl text-coklat font-medium'/>
                    <span className='text-xl text-coklat font-medium ml-4'>Skristanto7@gmail.com</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </footer>
  )
}

export default footer