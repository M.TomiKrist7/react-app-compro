import React, { useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router';

function navbar() {

  const [isShow,setShow] = useState(false);
  const router = useRouter();

  const showBar = () =>{
        setShow(!isShow)
  }


  return (
    
<nav className="bg-bee fixed w-full px-4 py-3 nb:py-3  sm:px-5 sm:py-6 kcl:py-5 rounded-b dark:bg-gray-900 z-10">
  <div className="w-full flex flex-wrap items-center justify-between mx-auto nb:mx-8">
    <Link href="home" className="flex items-center">
        <img src="/icon.png" className="h-6 mr-3 sm:h-9" alt="Flowbite Logo" />
        <span className="self-center text-2xl font-extrabold text-coklat whitespace-nowrap  dark:text-white">LET EAT BEE</span>
    </Link>
    <button data-collapse-toggle="navbar-default"  type="button" className="inline-flex items-center p-2 ml-3 mr-6 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="navbar-default" aria-expanded="false" onClick={showBar}>
      <span className="sr-only">Open main menu</span>
      <svg className="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd"></path></svg>

    </button>
    <div className={`${isShow ? '':'hidden'} w-full nb:block nb:w-auto pr-8`} id="navbar-default">
      <ul className="flex flex-col p-4 mt-4  rounded-lg bg-bee  nb:flex-row nb:space-x-8 nb:mt-0 nb:text-sm nb:font-medium nb:border-0 nb:bg-bee dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
        <li>
          <Link href="home"  className={`${router.pathname==='/home'?'text-gray-100':'text-coklat'} block py-2 pl-3 pr-4 text-xl font-bold rounded hover:text-gray-100 nb:hover:bg-transparent nb:border-0 nb:text-lg nb:hover:text-gray-100 nb:p-0 dark:text-gray-400 nb:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white nb:dark:hover:bg-transparent` }>Beranda</Link>
        </li>
        <li>
          <Link href="about" className={`${router.pathname==='/about'?'text-gray-100':'text-coklat'} block py-2 pl-3 pr-4 text-xl font-bold rounded hover:text-gray-100 nb:hover:bg-transparent nb:border-0 nb:text-lg nb:hover:text-gray-100 nb:p-0 dark:text-gray-400 nb:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white nb:dark:hover:bg-transparent` }>Tentang Kami</Link>
        </li>
        <li>
          <Link href="catalog" className={`${router.pathname==='/catalog'?'text-gray-100':'text-coklat'} block py-2 pl-3 pr-4 text-xl font-bold rounded hover:text-gray-100 nb:hover:bg-transparent nb:border-0 nb:text-lg nb:hover:text-gray-100 nb:p-0 dark:text-gray-400 nb:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white nb:dark:hover:bg-transparent` }>Katalog</Link>
        </li>
        <li>
          <Link href="product" className={`${router.pathname==='/product'?'text-gray-100':'text-coklat'} block py-2 pl-3 pr-4 text-xl font-bold rounded hover:text-gray-100 nb:hover:bg-transparent nb:border-0 nb:text-lg nb:hover:text-gray-100 nb:p-0 dark:text-gray-400 nb:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white nb:dark:hover:bg-transparent` }>Produk</Link>
        </li>
        <li>
          <Link href="contact" className={`${router.pathname==='/contact'?'text-gray-100':'text-coklat'} block py-2 pl-3 pr-4 text-xl font-bold rounded hover:text-gray-100 nb:hover:bg-transparent nb:border-0 nb:text-lg nb:hover:text-gray-100 nb:p-0 dark:text-gray-400 nb:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white nb:dark:hover:bg-transparent` }>Hubungi Kami</Link>
        </li>
      </ul>
    </div>
  </div>
  
</nav>

  )
}

export default navbar