import React from 'react'

function inputComponent({ value, label, name, placeholder, type, onChange }) {
  return (
    <div className='flex flex-col mb-5'>
    <label className='inline-block text-[#5f5f5f] mb-2 xl:text-xl lg:text-xl sm:text-lg kcl:text-lg'>Nama</label>
    <input onChange={handleChangeName(vName)} onInput={fieldDirtyName} onFocus={handleHideName} onBlur={handleHideName} value={vName} className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline focus:outline-blue-300' type="text" id="name" placeholder='Ketik Nama'></input>
    <div className="mt-2">
        <span className={`${isHideName ? 'invisible text-gray-100':'text-red-600'}  text-sm`}>mohon masukkan Nama</span>
    </div>
  </div>
  )
}

export default inputComponent