<div className='w-full rounded-lg bg-gray-100 shadow-lg'>
<div className='p-7'>
  <h1 className='m-auto text-center text-xl font-bold'>Filter dengan</h1>
  <div className='mt-5'>
    <div className='flex flex-col mb-4'>
      <label className='mb-3 text-lg font-bold'>Nama</label>
      <input className='shadow appearance-none border rounded-xl w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline focus:outline-blue-300'></input>
    </div>
    <div className='flex flex-col'>
      <label className='mb-3 text-lg font-bold'>Category</label>
      <div>
        <div className='mb-1'>
          <input className='w-3 h-3 rounded-full' type="checkbox"></input>
          <label className='ml-4'>Mix Fruit</label>
        </div>
        <div className='mb-1'>
          <input type="checkbox"></input>
          <label className='ml-4'>Extra Youghurt</label>
        </div>
        <div className='mb-1'>
          <input type="checkbox"></input>
          <label className='ml-4'>No SKM</label>
        </div>
      </div>
    </div>
  </div>
</div>
</div>