import Head from "next/head";
import Image from "next/image";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";
import Navbar from "@/components/navbar";
import Footer from "@/components/footer";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import { LocaleRouteMatcher } from "next/dist/server/future/route-matchers/locale-route-matcher";
import {truncate} from "@/helpers/global";


const inter = Inter({ subsets: ["latin"] });

export default function Home() {


  const vPictures = ['/Slide1.webp','/Slide2.webp','/Slide3.webp']

  const vNews = {
    0:{
      link:'/Berita1.webp',
      headline:'Harga Salad',
      berita:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' 
    },
    1:{
      link:'/Berita2.webp',
      headline:'Penimbun Salad',
      berita:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' 
    },
    2:{
      link:'/Berita3.webp',
      headline:'Salad basi Dijual',
      berita:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' 
    },
    3:{
      link:'/Berita1.webp',
      headline:'Salad Segar',
      berita:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' 
    },
  }





  return (
    <>
      <Head>
        <title>LET EAT BEE</title>
      </Head>
      <Navbar />
      <main className="flex flex-col justify-between items-center pt-20 min-h-screen ">
        <Carousel className="" autoPlay={true} showThumbs={false} showStatus={false}>
          {vPictures.map((item)=>(
            <div key={item.id} className="xl:h-[34rem] lg:h-[34rem] sm:h-44 kcl:h-44">
                <img className="xl:h-full lg:h-full sm:h-44 kcl:h-44 "  src={item} />
            </div>

          ))}

        </Carousel>
        <div className="h-auto xl:py-10 lg:py-10 sm:py-5 kcl:py-5">
          <div className="w-2/3 p-2 m-auto">
            <h2 className="text-center text-coklat xl:text-xl lg:text-xl sm:text-lg kcl:text-lg font-extrabold mt-9 mb-5">
              Salad Buah Segar setiap Hari
            </h2>
            <p className="text-center text-coklat xl:text-lg lg:text-lg sm:text-sm kcl:text-sm font-semibold">
              Let Eat Bee menyediakan salad segar setiap hari untuk kebutuhan
              gizi dan vitamin anda. Salad kami dibuat dengan buah-buahan segar
              yang dipanen dari kebun buah organik kami. Lalu diproses dengan
              tangan-tangan profesional yang menghasilkan salad buah nikmat nan
              menyehatkan
            </p>
          </div>


          <div>
            <h2 className="text-center text-coklat font-extrabold mt-9 mb-5 xl:text-xl lg:text-xl sm:text-lg kcl:text-lg">
              Keunggulan Let Eat Bee
            </h2>
            <div className="flex  lg:justify-evently md:justify-center sm:justify-center lg:gap-8 md:gap-5 sm:gap-4 kcl:gap-4 justify-center items-center flex-wrap h-full g-1">
              <div className="w-[22rem] h-52 shadow-lg rounded ">
                <div className="p-4">
                  <div className="flex">
                    <img className="h-7 w-7" src="/strawberry.png" />
                    <h3 className=" text-coklat text-lg font-extrabold ml-5">
                      Bahan 100% Alami
                    </h3>
                  </div>
                  <p className="text-coklat font-medium mt-3 ml-3 leading-5">
                    Salad kami dibuat dengan buah-buahan segar yang dipanen dari
                    kebun buah organik kami. Lalu diproses dengan tangan-tangan
                    profesional yang menghasilkan salad buah nikmat nan
                    menyehatkan
                  </p>
                </div>
              </div>
              <div className="w-[22rem] h-52 shadow-lg rounded bg-gradient-to-r from-[#fdeda5] via-[#fce26d] to-[#fcd423]">
                <div className="p-4">
                  <h3 className=" text-coklat text-lg font-extrabold leading-5">
                    Segera kunjungi toko kami atau Hubungi kami untuk pemesanan
                  </h3>
                  <div className="w-full">&nbsp;</div>
                </div>
              </div>
            </div>
          </div>



        </div>
        <div className="h-2 md:h-2 sm:h-2 kcl:h-2 w-full bg-bee"></div>

        <div className="h-auto pt-4 pb-12 ">
          <div className="text-center text-coklat text-xl font-extrabold mt-9 mb-5">Berita</div>
            <div  className="flex  lg:justify-between md:justify-center sm:justify-center lg:gap-8 md:gap-5 sm:gap-4 kcl:gap-4 justify-center flex-wrap h-full g-1">
          {Object.keys(vNews).map((label,index)=>(
                
                  <div key={index} className="h-80 overflow-hidden shadow-2xl rounded-lg transition duration-300 ease-in hover:scale-105 xl:w-48  lg:w-48  sm:w-44  kcl:w-44 ">
                    <div className="">
                      <img src={vNews[label].link} className="min-h-[210px] h-[220px] w-full object-fill rounded-t-lg"></img>
                    </div>
                    <div className=" bg-bee px-2 py-2 h-36 ">
                      <div className="p-1">
                        <div className="text-xl font-semibold text-coklat">{vNews[label].headline}</div>
                        <div className="mt-1 text-sm text-coklat overflow-hidden">{truncate(vNews[label].berita,40) }</div>
                      </div>
                    </div>
                  </div>
                

          ))}
          </div>
        </div>

        <div className="h-4 md:h-4 sm:h-4 kcl:h-4 w-full bg-coklat"></div>
      </main>
      <Footer/>
    </>
  );
}
