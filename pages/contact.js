import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import Navbar from '@/components/navbar'
import Footer from '@/components/footer'
import { useState,useEffect } from 'react'
import { set, useForm, useFormState } from 'react-hook-form';
import Link from 'next/link'
import { AiFillHome } from "react-icons/ai";

const inter = Inter({ subsets: ['latin'] })

export default function Contac() {

  const {
    register,
    handleSubmit,
    getValues,
    control,
    formState: { errors }
  } = useForm({
    defaultValues:{
      name:""
    }
  });

  const { dirtyFields } = useFormState({
    control
  });

  let[isChecking, setChecking] = useState(false)

  const[vName,setName]=useState("")
  const[vPhone,setPhone]=useState("")
  const[vEmail,setEmail]=useState("")
  const[vCompany,setCompany]=useState("")
  const[vCountry,setCountry]=useState("")
  const[vMessage,setMessage]=useState("")


  const[isHideName, setHideName] = useState(true)
  const[isHidePhone, setHidePhone] = useState(true)
  const[isHideEmail, setHideEmail] = useState(true)
  const[isHideCompany, setHideCompany] = useState(true)
  const[isHideCountry, setHideCounty] = useState(true)
  const[isHideMessage, setHideMessage] = useState(true)


  const[nameDirty,setNameDirty]=useState(false)
  const[phoneDirty,setPhoneDirty]=useState(false)
  const[emailDirty,setEmailDirty]=useState(false)
  const[companyDirty,setCompanyDirty]=useState(false)
  const[countryDirty,setCountryDirty]=useState(false)
  const[messageDirty,setMessageDirty]=useState(false)


  useEffect(()=>{
		setHideName(true)
	}, [])



//Control Name

  const fieldDirtyName =()=>{
    if(nameDirty===false){
      setNameDirty(!nameDirty);
      return
    }

  }

  const handleHideName = () =>{
    if(nameDirty===true && vName===""){
      setHideName(!isHideName)
      return 
    }
  }

  const handleChangeName = ()=> (event) =>{
    setName(event.target.value)
  }

//End Control Name

//Control Phone

  const fieldDirtyPhone =()=>{
    if(phoneDirty===false){
      setPhoneDirty(!phoneDirty);
      return
    }
  }

  const handleHidePhone = () =>{
    if(phoneDirty===true && vPhone===""){
      setHidePhone(!isHidePhone)
      return
    }
  }

  const handleChangePhone = ()=> (event) =>{
    setPhone(event.target.value)
  }

//End Control Phone

//Control Email
const fieldDirtyEmail =()=>{
  if(emailDirty===false){
    setEmailDirty(!emailDirty);
    console.log(emailDirty)
    return
  }
}

const handleHideEmail = () =>{
  if(emailDirty===true && vEmail===""){
    setHideEmail(!isHideEmail)
    return
  }
}

const handleChangeEmail = ()=> (event) =>{
  setEmail(event.target.value)
}

//End Control Email  

//Control Company

  const fieldDirtyCompany =()=>{
    if(companyDirty===false){
      setCompanyDirty(!companyDirty);
      return
    }

  }

  const handleHideCompany = () =>{
    if(companyDirty===true && vCompany===""){
      setHideCompany(!isHideCompany)
      return
    }
  }

  const handleChangeCompany = ()=> (event) =>{
    setCompany(event.target.value)
  }

//End Control Company

//Control Country
  const fieldDirtyCountry =()=>{
    if(countryDirty===false){
      setCountryDirty(!countryDirty);
      return
    }
  }

  const handleHideCountry = () =>{
    if(countryDirty===true && vCountry===""){ 
      setHideCounty(!isHideCountry)
      return
    }
  }

  const handleChangeCountry = ()=> (event) =>{
    setCountry(event.target.value)
  }

//End Control Country

//Control Message
  const fieldDirtyMessage =()=>{
    if(messageDirty===false){
      setMessageDirty(!messageDirty);
      return
    }
  }

  const handleHideMessage = () =>{
    if(messageDirty===true && vMessage===""){
      setHideMessage(!isHideMessage)
      return
    }
  }

  const handleChangeMessage = ()=> (event) =>{
    setMessage(event.target.value)
  }

//End Control Message



  const onSubmit = (data) => {
   alert("Sukses!")
  };  


  return (
    <>
      <Head>
        <title>LET EAT BEE</title>
      </Head>
      <Navbar/>
      <main className='flex flex-col justify-between items-center pt-20 min-h-screen w-auto'>
        <div className='relative block w-auto h-auto'>
              <div className=' static'>
              <img className='opacity-25 max-w-screen' src='/bg2.webp' alt='background' />
                <div className='xl:w-80 lg:w-80 flex flex-col justify-between absolute h-auto text-center xl:bottom-1/3 lg:bottom-1/3 xl:left-[20%] lg:left-[20%] sm:bottom-[15%] sm:left-[5%] sm:w-44 kcl:w-44 kcl:bottom-[15%] kcl:left-[5%]'>
                  <div className='xl:w-[50rem] lg:w-[50rem] sm:w-96 kcl:w-96'>
                    <h1 className='text-coklat text-4xl xl:text-4xl lg:text-4xl md:text-2xl  sm:text-lg kcl:text-lg font-extrabold xl:mb-4 lg:mb-4 sm:mb-1 kcl:mb-1'>Kami Siap Menjawab Pertanyaan Anda.</h1>
                    <p className='text-coklat text-xl xl:text-xl lg:text-xl md:text-lg  sm:text-xs kcl:text-xs'>Silahkan untuk menuliskan pertanyaan mengenai produk dan layanan PT LET EAT BEE pada kolom di bawah ini. Kami akan segera menjawab pertanyaan anda. Terima kasih</p>
                  </div>
                </div>
              </div>
          </div>
          <div className='h-auto w-full pb-9 '>
              <form onSubmit={handleSubmit(onSubmit)} className='w-4/5 h-auto m-auto rounded-lg shadow-2xl p-12 bg-gray-100'>
                <div className='flex flex-col mb-5'>
                  <label className='inline-block text-[#5f5f5f] mb-2 xl:text-xl lg:text-xl sm:text-lg kcl:text-lg'>Nama</label>
                  <input onChange={handleChangeName(vName)} onInput={fieldDirtyName} onFocus={handleHideName} onBlur={handleHideName} value={vName} className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline focus:outline-blue-300' type="text" id="name" placeholder='Ketik Nama'></input>
                  <div className="mt-2">
                      <span className={`${isHideName ? 'invisible text-gray-100':'text-red-600'}  text-sm`}>mohon masukkan Nama</span>
                  </div>
                </div>
                <div className='flex flex-row justify-between  mb-5  xl:gap-8 lg:gap-8 xl:flex-nowrap lg:flex-nowrap sm:flex-wrap kcl:flex-wrap sm:gap-4 kcl:gap-4 sm:justify-center sm:items-center kcl:justify-center kcl:items-center'>
                  <div className='flex flex-col  xl:w-2/4 lg:w-2/4 sm:w-full kcl:w-full'>
                    <label className='inline-block text-[#5f5f5f] mb-2 xl:text-xl lg:text-xl sm:text-lg kcl:text-lg'>No. Handphone</label>
                    <input onChange={handleChangePhone(vPhone)} onInput={fieldDirtyPhone} onFocus={handleHidePhone} onBlur={handleHidePhone} value={vPhone} className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline focus:outline-blue-300' type="text" id="phone" placeholder='Ketik No. Handphone'></input>
                    <div className="mt-2">
                        <span className={`${isHidePhone ? 'invisible text-gray-100':'text-red-600'} text-sm`}>mohon masukkan Nomor Telepon</span>
                    </div>
                 </div>
                  <div className='flex flex-col xl:w-2/4 lg:w-2/4 sm:w-full kcl:w-full'>
                    <label className='inline-block text-[#5f5f5f] mb-2 xl:text-xl lg:text-xl sm:text-lg kcl:text-lg'>Kontak Email</label>
                    <input onChange={handleChangeEmail(vEmail)} onInput={fieldDirtyEmail} onFocus={handleHideEmail} onBlur={handleHideEmail} value={vEmail} className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline focus:outline-blue-300' type="email" id="email" placeholder='Ketik Email'></input>
                    <div className="mt-2">
                        <span className={`${isHideEmail ? 'invisible text-gray-100':'text-red-600'} text-sm`}>mohon masukkan Email</span>
                    </div>
                  </div>
                </div>
                <div className='flex flex-row justify-between  mb-5  xl:gap-8 lg:gap-8 xl:flex-nowrap lg:flex-nowrap sm:flex-wrap kcl:flex-wrap sm:gap-4 kcl:gap-4 sm:justify-center sm:items-center kcl:justify-center kcl:items-center'>
                  <div className='flex flex-col w-2/4 xl:w-2/4 lg:w-2/4 sm:w-full kcl:w-full'>
                    <label className='inline-block text-[#5f5f5f] mb-2 xl:text-xl lg:text-xl sm:text-lg kcl:text-lg'>Nama Perusahaan</label>
                    <input onChange={handleChangeCompany(vCompany)} onInput={fieldDirtyCompany} onFocus={handleHideCompany} onBlur={handleHideCompany} value={vCompany} className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline focus:outline-blue-300' type="text" id="company" placeholder='Ketik Nama Perusahaan'></input>
                    <div className="mt-2">
                        <span className={`${isHideCompany ? 'invisible text-gray-100':'text-red-600'} text-sm`}>mohon masukkan Nama Perusahaan</span>
                    </div>
                  </div>
                  <div className='flex flex-col w-2/4 xl:w-2/4 lg:w-2/4 sm:w-full kcl:w-full'>
                    <label className='inline-block text-[#5f5f5f] mb-2 xl:text-xl lg:text-xl sm:text-lg kcl:text-lg'>Negara</label>
                    <input onChange={handleChangeCountry(vCountry)} onInput={fieldDirtyCountry} onFocus={handleHideCountry} onBlur={handleHideCountry} value={vCountry} className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline focus:outline-blue-300' type="text" id="country" placeholder='Ketik Negara'></input>
                    <div className="mt-2">
                        <span className={`${isHideCountry ? 'invisible text-gray-100':'text-red-600'} text-sm`}>mohon masukkan Negara</span>
                    </div>
                  </div>
                </div>
                <div className='flex flex-col mb-5'>
                  <label className='inline-block text-[#5f5f5f] mb-2 xl:text-xl lg:text-xl sm:text-lg kcl:text-lg'>Pesan</label>
                  <textarea onChange={handleChangeMessage(vMessage)} onInput={fieldDirtyMessage} onFocus={handleHideMessage} onBlur={handleHideMessage} value={vMessage}  className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline focus:outline-blue-300' type="text" id="message" placeholder='Ketik Pesan ' cols="10" rows="5"></textarea>
                  <div className="mt-2">
                    <span className={`${isHideMessage ? 'invisible text-gray-100':'text-red-600'} text-sm`}>mohon isi Pesan</span>
                  </div>
                </div>
                <div className='mt-16'>
                  <button className='bg-transparent border border-[#5f5f5f] hover:border-bee hover:text-bee text-[#5f5f5f] font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'>Kirim</button>
                </div>
              </form>
            </div>
      </main>
      <Footer/>
    </>
  )
}