import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import Navbar from '@/components/navbar'
import Footer from '@/components/footer'
import Link from 'next/link'
import { Item } from '@/helpers/global'
import {truncate} from "@/helpers/global";
import { AiFillHome } from "react-icons/ai";
import { IoIosArrowForward } from "react-icons/Io";
import { useEffect, useMemo, useState } from 'react'

const inter = Inter({ subsets: ['latin'] })

export default function Catalog() {

  // const[vName,setName] = useState("")
  // const[filtered,setFiltered]=useState("")
  // const[mix,setMix] = useState("N")
  // const[items,setItems] = useState([])

  const [query,setQuery] = useState("")



  useEffect(()=>{

	}, [])

  const handleChange = ()=> (e) =>{
    setFiltered(e.target.value)
    console.log(filtered);

  }



  return (
    <>
      <Head>
        <title>LET EAT BEE</title>
      </Head>
      <Navbar/>
      <main className='flex flex-col justify-between items-center pt-20 min-h-screen w-auto '>
      <section className='relative block h-auto'>
            <div className=' static'>
              <img className='opacity-25 max-w-screen' src='/katalog.webp' alt='background' />
              <div className='xl:w-80 lg:w-80 flex flex-col justify-between absolute h-auto xl:bottom-2/4 lg:bottom-2/4 xl:left-[5%] lg:left-[5%] sm:bottom-1/4 sm:left-[5%] sm:w-44 kcl:w-44 kcl:bottom-1/4 kcl:left-[5%]'>
                <Link href="home" className='text-coklat text-lg w-16 flex justify-between xl:mb-5 lg:mb-5 sm:mb-1 kcl:mb-1 gap-4 hover:text-bee' >
                  <p className='text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-sm kcl:text-sm '>Beranda</p>
                  <p className=' pt-1'><AiFillHome className=' font-bold text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-sm kcl:text-sm'/></p>
                </Link>
                <h1 className='text-coklat text-4xl xl:text-4xl lg:text-4xl md:text-2xl  sm:text-xl kcl:text-xl font-extrabold xl:mb-4 lg:mb-4 sm:mb-1 kcl:mb-1'>Katalog</h1>
                <p className='text-coklat text-xl xl:text-xl lg:text-xl md:text-lg  sm:text-sm kcl:text-sm'>Katalog Produk </p>
              </div>
            </div>
        </section>
        <section className="w-full p-9">
          <div className='m-auto rounded-xl shadow-lg bg-gray-100 w-full'>
            <div className='w-full h-16 flex flex-row p-2 mx-auto '>
              <div className='m-auto p-3 w-full border-b border-b-black'>
                <div className='flex flex-row justify-between items-center  gap-28 '>
                    <div className='w-52 flex flex-row gap-9'>
                      <label className='w-16 py-1 font-bold text-lg'>Cari</label>
                      <input onChange={(e)=>setQuery(e.target.value)}  className='w-60 shadow appearance-none border rounded py-1 px-1 text-gray-700 leading-tight focus:outline-none focus:shadow-outline focus:outline-blue-300'></input>
                    </div>
                  <div className='flex flex-row gap-8'>
                    <label className='font-bold text-lg'>Category</label>
                    <div className='flex flex-row gap-6 py-1'>
                      <div className=''>
                        <input  className='mr-2' type="checkbox"></input>
                        <label className=''>Mix</label>
                      </div>
                      <div>
                        <input className='mr-2' type="checkbox"></input>
                        <label className=''>No SKM</label>
                      </div>
                      <div>
                        <input className='mr-2' type="checkbox"></input>
                        <label className=''>Ekstra Mayo</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className="h-auto pt-10 pb-12 px-7 ">
                <div  className="flex  lg:justify-start md:justify-center sm:justify-center lg:gap-8 md:gap-5 sm:gap-4 kcl:gap-4 justify-center flex-wrap h-full g-1">
                  {Item.filter((item)=>item.nama.toLowerCase().includes(query)).map((item)=>(
                    <div key={item.id} className="h-80 overflow-hidden shadow-2xl rounded-lg transition duration-300 ease-in hover:scale-105 xl:w-52  lg:w-52  sm:w-44  kcl:w-44 ">
                      <div className="">
                        <img src={item.link} className="min-h-[180px] h-[190px] w-full object-fill rounded-t-lg"></img>
                      </div>
                      <div className=" bg-white px-2 py-2 h-36 ">
                        <div className="p-1">
                          <div className="text-xl font-semibold text-coklat">{item.nama}</div>
                          <div className="mt-1 text-sm text-coklat overflow-hidden">{truncate(item.deskripsi,40) }</div>
                       </div>
                     </div>
                    </div>
                  ))}
               </div>
            </div>
            </div>
          </div>
        </section>
      </main>
      <Footer/>
    </>
  )
}