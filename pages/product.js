import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import Link from 'next/link'
import { AiFillHome } from "react-icons/ai";
import Navbar from '@/components/navbar'
import Footer from '@/components/footer'

const inter = Inter({ subsets: ['latin'] })

export default function Product() {
  return (
    <>
      <Head>
        <title>LET EAT BEE</title>
      </Head>
      <Navbar/>
      <main className='flex flex-col justify-between items-center pt-20 min-h-screen w-auto '>
      <section className='relative block h-auto'>
            <div className=' static'>
              <img className='opacity-25 max-w-screen' src='/katalog.webp' alt='background' />
              <div className='xl:w-80 lg:w-80 flex flex-col justify-between absolute h-auto xl:bottom-2/4 lg:bottom-2/4 xl:left-[5%] lg:left-[5%] sm:bottom-1/4 sm:left-[5%] sm:w-44 kcl:w-44 kcl:bottom-1/4 kcl:left-[5%]'>
                <Link href="home" className='text-coklat text-lg w-16 flex justify-between xl:mb-5 lg:mb-5 sm:mb-1 kcl:mb-1 gap-4 hover:text-bee' >
                  <p className='text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-sm kcl:text-sm '>Beranda</p>
                  <p className=' pt-1'><AiFillHome className=' font-bold text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-sm kcl:text-sm'/></p>
                </Link>
                <h1 className='text-coklat text-4xl xl:text-4xl lg:text-4xl md:text-2xl  sm:text-xl kcl:text-xl font-extrabold xl:mb-4 lg:mb-4 sm:mb-1 kcl:mb-1'>Barang</h1>
                <p className='text-coklat text-xl xl:text-xl lg:text-xl md:text-lg  sm:text-sm kcl:text-sm'>Katalog Produk </p>
              </div>
            </div>
        </section>
        <section className='relative block h-auto'>
            <div className=' static'>
              <img className='opacity-25 max-w-screen' src='/bg2.webp' alt='background' />
              <div className='xl:w-80 lg:w-80 flex flex-col justify-between absolute h-auto xl:bottom-2/4 lg:bottom-2/4 xl:left-[5%] lg:left-[5%] sm:bottom-1/4 sm:left-[5%] sm:w-44 kcl:w-44 kcl:bottom-1/4 kcl:left-[5%]'>
                <Link href="home" className='text-coklat text-lg w-16 flex justify-between xl:mb-5 lg:mb-5 sm:mb-1 kcl:mb-1 gap-4 hover:text-bee' >
                  <p className='text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-sm kcl:text-sm '>Beranda</p>
                  <p className=' pt-1'><AiFillHome className=' font-bold text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-sm kcl:text-sm'/></p>
                </Link>
                <h1 className='text-coklat text-4xl xl:text-4xl lg:text-4xl md:text-2xl  sm:text-xl kcl:text-xl font-extrabold xl:mb-4 lg:mb-4 sm:mb-1 kcl:mb-1'>Jasa</h1>
                <p className='text-coklat text-xl xl:text-xl lg:text-xl md:text-lg  sm:text-sm kcl:text-sm'>Informasi Penjualan</p>
              </div>
            </div>
        </section>
      </main>
      <Footer/>
    </>
  )
}