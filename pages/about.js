import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import Link from 'next/link'
import Navbar from '@/components/navbar'
import Footer from '@/components/footer'
import { AiFillHome } from "react-icons/ai";
import { IoIosArrowForward } from "react-icons/Io";


const inter = Inter({ subsets: ['latin'] })

export default function About() {
  return (
    <>
      <Head>
        <title>LET EAT BEE</title>
      </Head>
      <Navbar/>
      <main className="flex flex-col justify-between items-center pt-20 min-h-screen w-auto">
        <section className='relative block h-auto'>
            <div className=' static'>
              <img className='opacity-25 max-w-screen' src='/bg2.webp' alt='background' />
              <div className='xl:w-80 lg:w-80 flex flex-col justify-between absolute h-auto xl:bottom-2/4 lg:bottom-2/4 xl:left-[5%] lg:left-[5%] sm:bottom-1/4 sm:left-[5%] sm:w-44 kcl:w-44 kcl:bottom-1/4 kcl:left-[5%]'>
                <Link href="home" className='text-coklat text-lg w-16 flex justify-between xl:mb-5 lg:mb-5 sm:mb-1 kcl:mb-1 gap-4 hover:text-bee' >
                  <p className='text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-sm kcl:text-sm '>Beranda</p>
                  <p className=' pt-1'><AiFillHome className=' font-bold text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-sm kcl:text-sm'/></p>
                </Link>
                <h1 className='text-coklat text-4xl xl:text-4xl lg:text-4xl md:text-2xl  sm:text-xl kcl:text-xl font-extrabold xl:mb-4 lg:mb-4 sm:mb-1 kcl:mb-1'>Tentang Kami</h1>
                <p className='text-coklat text-xl xl:text-xl lg:text-xl md:text-lg  sm:text-sm kcl:text-sm'>Informasi Penjualan</p>
              </div>
            </div>
        </section>
        <section>
          <div className='m-auto p-5 pt-24'>
            <div className='h-auto w-4/5 m-auto'>
              <img className='rounded-lg shadow-xl' src='/pp.webp' alt="makmur"/>
            </div>
            <div className='m-auto w-4/5'>
              <h2 className='text-coklat text-xl font-extrabold mt-8'>Informasi Perusahaan</h2>
              <p className='py-5 text-coklat'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Pellentesque vel sapien aliquam, sodales quam id, suscipit nunc. 
                Fusce vel felis dignissim, varius augue ut, tristique nulla. 
                Nulla facilisi. Ut et leo sem. 
                Morbi maximus, nisl in lobortis auctor, leo lectus tincidunt ipsum,
                 sed interdum nulla lectus vel nunc. 
                 Etiam tortor nibh, feugiat eget ligula sed, tristique tristique ex. 
                 Aliquam dapibus dignissim ipsum eget laoreet. 
                 Vestibulum hendrerit, ex id finibus semper, lectus eros egestas justo, 
                 interdum maximus velit neque ut mauris. 
                 Aenean vestibulum sapien blandit mattis malesuada. 
                 Proin eu interdum diam, at fermentum massa. 
                 Curabitur nec sagittis arcu, at ultricies dui.
              </p>
              <p className='text-coklat'>
               In tincidunt dignissim magna, ut fringilla tellus ullamcorper vitae. 
               Etiam fermentum justo et nunc consequat dictum.
                Aenean ullamcorper sem eu quam sollicitudin, eget fringilla eros efficitur. 
                Etiam vitae erat vel mauris posuere aliquet sed sed augue. 
                Morbi pellentesque purus vel odio interdum, at ultrices nunc scelerisque.
                Nullam sed arcu ac massa vehicula fermentum. Ut sit amet leo sit amet nisi malesuada semper. 
                Nulla aliquam ac dui eu cursus.
              </p>
              <p className='py-5 text-coklat'>
                Aenean vitae tortor ac enim sagittis interdum. 
                Vivamus faucibus dignissim condimentum. 
                Ut at diam lorem. Aenean iaculis hendrerit nunc, in congue erat. 
                Praesent eu sem quis diam dignissim tincidunt. 
                Quisque consequat laoreet diam, eget volutpat massa congue sit amet. 
                Vestibulum gravida velit est, ut convallis lectus congue eu
              </p>
            </div>
            <div className='m-auto py-12 w-4/5'>
              <div className='flex justify-between items-center '>
                <div className='w-52 xl:w-52 lg:w-52 md:w-40 nb:w-40 sm:w-20 kcl:w-20'>
                  <ul className='flex flex-col justify-between '>
                    <Link  href="catalog">
                      <li className=' flex justify-between items-center border-b-[1px] text-coklat border-b-coklat hover:text-bee hover:border-b-bee'>
                        <span className='text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-xs kcl:text-xs'>Katalog</span>
                        <span className='text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-xs kcl:text-xs pt-1 m-0'><IoIosArrowForward/></span>
                      </li>
                    </Link>
                  </ul>
                </div>
                <div className='w-52 xl:w-52 lg:w-52 md:w-40 nb:w-40 sm:w-20 kcl:w-20'>
                  <ul className='flex flex-col justify-between '>
                    <Link  href="product">
                      <li className=' flex justify-between items-center border-b-[1px] text-coklat border-b-coklat hover:text-bee hover:border-b-bee'>
                        <span className='text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-xs kcl:text-xs'>Produk</span>
                        <span className='text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-xs kcl:text-xs pt-1 m-0'><IoIosArrowForward/></span>
                      </li>
                    </Link>
                  </ul>
                </div>
                <div className='w-52 xl:w-52 lg:w-52 md:w-40 nb:w-40 sm:w-24 kcl:w-24'>
                  <ul className='flex flex-col justify-between '>
                    <Link  href="contact">
                      <li className=' flex justify-between items-center border-b-[1px] text-coklat border-b-coklat hover:text-bee hover:border-b-bee'>
                        <span className='text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-xs kcl:text-xs'>Hubungi Kami</span>
                        <span className='text-xl xl:text-xl lg:text-xl md:text-lg nb:text-lg sm:text-xs kcl:text-xs pt-1 m-0'><IoIosArrowForward/></span>
                      </li>
                    </Link>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <Footer/>
    </>
  )
}